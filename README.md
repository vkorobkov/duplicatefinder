# DuplicateFinder

What's that? Just a small program which's been designing for searching files with the same crc32 (e.g. potentially binary equaled files). Should handle million of files, so, the main goal - extra low memory consumption (as best as I can in Java at least)

## I've done testing on environment with 330 thousands of files. That's only the initial testing.

## Proof on concept. No optimizations, just working prototype. Used to take 215 of 256MB of heap(on 330k files)

![1.png](https://bitbucket.org/repo/rjbxRy/images/966833399-1.png)


## Final(for now) and optimized is working under 96MB of heap:

![2.png](https://bitbucket.org/repo/rjbxRy/images/2721194289-2.png)

## How that was achived? Several hacks and dirty code. Do not repeat that at home.

* Decreased usage of HashMap as a most heavy collection It was partly replaced with an array for small files. See com.vkorobkov.duplicate.GroupedBySizeFilesStorage

* I got rid of LinkedList. At all. Introduced super-extra ligh weight queue collection: com.vkorobkov.duplicate.util.FastQueue

* Specialized collections usage(google "trove java"). Avoids redundant boxing/unboxing and avoids existense of primitive type wrappers such as Long.


# # Is that all? No! The fun has just started. Plans:

* Deep dive into unmanaged memory + Unsafe class. Using memory off the heap would be a great plus
* Come general stuff like parsing command line arguments instead of hardcoding it(like now)
* Multithreading(in case there are severl physically different drives/disks)


Thanks, Vladimir (vladimir.korobkov[@]gmail.com)