package com.vkorobkov.duplicate;

import com.vkorobkov.duplicate.util.CrcCalculator;
import com.vkorobkov.duplicate.util.FastQueue;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongObjectHashMap;

import java.io.IOException;

public class SimilarFilesProcessor {

    TLongObjectMap<FastQueue<String>> extractSimilarFiles(Iterable<String> files, ErrorCallback errorCallback) {
        TLongObjectMap<FastQueue<String>> similarFiles = new TLongObjectHashMap<>();
        for(String file: files) {
            try {
                long crc = CrcCalculator.getFileCrc(file);
                FastQueue<String> list = similarFiles.get(crc);
                if (list == null) {
                    similarFiles.put(crc, FastQueue.fromValues(file));
                }
                else {
                    list.add(file);
                }
            } catch (IOException exception) {
                errorCallback.onError(file, exception);
            }
        }
        return similarFiles;
    }

    public interface ErrorCallback {
        void onError(String file, IOException exception);
    }
}
