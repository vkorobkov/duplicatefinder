package com.vkorobkov.duplicate;

import com.vkorobkov.duplicate.util.FastQueue;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongObjectHashMap;

import java.util.Iterator;

public class GroupedBySizeFilesStorage implements Iterable<Iterable<String>> {

    // Because the most files are smaller than 32KB, I would avoid them to be presenting in the hashmap.
    // HashMap brings too much overhead. I put this files just into array and the file size is the index
    private final static int SMALL_FILES_LIMIT = 1024 * 32;

    private final Object[] SMALL_FILES = new Object[SMALL_FILES_LIMIT];

    private final TLongObjectMap<FastQueue<String>> bigFiles = new TLongObjectHashMap<>();

    public void add(long size, String path) {
        if (size < SMALL_FILES_LIMIT) {
            addToSmallFiles(size, path);
        }
        else {
            addToBigFiles(size, path);
        }
    }

    private void addToSmallFiles(long size, String path) {
        int intSize = (int)size;

        Object existingValue = SMALL_FILES[intSize];

        // There is a trick here. If there is only one file for specified file size, we do not create
        // FastQueue. We create it once we have two or more elements to add
        if (existingValue instanceof FastQueue) {
            ((FastQueue<String>)existingValue).add(path);
        }
        else if (existingValue instanceof String) {
            SMALL_FILES[intSize] = FastQueue.fromValues((String)existingValue, path);
        }
        else {
            SMALL_FILES[intSize] = FastQueue.fromValues(path);
        }
    }

    private void addToBigFiles(long size, String path) {
        if (bigFiles.containsKey(size)) {
            ((FastQueue)bigFiles.get(size)).add(path);
        }
        else {
            FastQueue<String> value = new FastQueue<>();
            value.add(path);
            bigFiles.put(size, value);
        }
    }

    @Override
    public Iterator<Iterable<String>> iterator() {
        final Iterator<FastQueue<String>> bigFilesIterator = bigFiles.valueCollection().iterator();

        return new Iterator<Iterable<String>>() {
            private boolean hasSmallFilesIterated = false;
            private int smallFilesIndex = 0;

            @Override
            public boolean hasNext() {
                if (!hasSmallFilesIterated) {
                    return true;
                }
                return bigFilesIterator.hasNext();
            }

            @Override
            public Iterable<String> next() {
                if (!hasSmallFilesIterated) {
                    Iterable<String> result;
                    do {
                        result = getSmallFilesAsFastQueue(smallFilesIndex);
                        smallFilesIndex++;
                        hasSmallFilesIterated = smallFilesIndex >= SMALL_FILES_LIMIT;
                    } while (!hasSmallFilesIterated && result == null);
                    if (result != null) {
                        return result;
                    }
                }
                return bigFilesIterator.next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            private FastQueue<String> getSmallFilesAsFastQueue(int index) {
                Object result = SMALL_FILES[smallFilesIndex];
                if (result instanceof String) {
                    return FastQueue.fromValues((String)result);
                }
                else {
                    return (FastQueue<String>)result;
                }
            }
        };
    }
}
