package com.vkorobkov.duplicate;

import com.vkorobkov.duplicate.util.DirectoryScanner;

import java.io.File;
import java.io.IOException;

public class FileScannerCallbackImpl implements DirectoryScanner.FileScannerCallback {

    private final boolean sendErrorsToOutput;

    private final GroupedBySizeFilesStorage storage;

    public FileScannerCallbackImpl(boolean sendErrorsToOutput, GroupedBySizeFilesStorage storage) {
        this.sendErrorsToOutput = sendErrorsToOutput;
        this.storage = storage;
    }

    @Override
    public void onFileFound(File file) {
        long size = file.length();
        try {
            storage.add(size, file.getCanonicalPath());
        } catch (IOException e) {
            if (sendErrorsToOutput) {
                System.err.println("Error processing file: " + file);
            }
        }
    }

    @Override
    public void onError(File file) {
        if (sendErrorsToOutput) {
            System.err.println("Error processing file: " + file);
        }
    }
}
