package com.vkorobkov.duplicate.util;

import java.util.Arrays;
import java.util.Iterator;

/*
 * The queue with the smallest memory consumption. Ever. Of course, in terms of managed memory.
 *
 * Does support adding elements, iterating and getting size.
 *
 * Working with unmanaged memory(hello Unsafe) is an idea for one more topic
 */
public class FastQueue<T> implements Iterable<T> {

    // In order to save memory using array for holding value + reference to the next element
    private Object[] first;

    private int size = 0;

    public static<T> FastQueue<T> fromValues(T ... values) {
        FastQueue<T> result = new FastQueue<T>();
        for (T value: values) {
            result.add(value);
        }
        return result;
    }

    public void add(T value) {
        ++size;

        if (first == null) {
            updateFirst(value, null);
        }
        else {
            updateFirst(value, Arrays.copyOf(first, 2));
        }
    }

    public int size() {
        return size;
    }

    private void updateFirst(T value, Object next) {
        first = new Object[2];
        first[0] = value;
        first[1] = next;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private Object[] localFirst = first;

            @Override
            public boolean hasNext() {
                return localFirst != null;
            }

            @Override
            public T next() {
                T result = (T)localFirst[0];
                localFirst = (Object[])localFirst[1];
                return result;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}
