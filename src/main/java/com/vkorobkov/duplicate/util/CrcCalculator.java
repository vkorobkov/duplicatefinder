package com.vkorobkov.duplicate.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.CRC32;

public class CrcCalculator {

    private static final int BUFFER_SIZE = 1024 * 1024 * 1; // 1 MB cache size
//    private static final int BUFFER_SIZE = 1024 * 1; // test cache cache size

    private final static CRC32 crc32 = new CRC32();
    private final static byte[] buffer = new byte[BUFFER_SIZE];

    public static long getFileCrc(String path) throws IOException {
        crc32.reset();
        try (InputStream inputStream = new FileInputStream(path)) {
            int bytesRead;
            while((bytesRead = inputStream.read(buffer)) != -1) {
                crc32.update(buffer, 0, bytesRead);
                break; // just for debug purposes; remove be later
            }
            return crc32.getValue();
        }
    }
}
