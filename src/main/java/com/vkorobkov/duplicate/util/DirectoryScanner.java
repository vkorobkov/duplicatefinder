package com.vkorobkov.duplicate.util;


import java.io.File;

public class DirectoryScanner {

    private final File rootPath;
    private final FileScannerCallback callback;

    public DirectoryScanner(String rootPath, FileScannerCallback callback) {
        this.rootPath = new File(rootPath);
        this.callback = callback;
    }

    public void scan() {
        scanRecursively(rootPath);
    }

    private void scanRecursively(File path) {
        if (path.isDirectory()) {
            processDirectory(path);
        }
        else {
            callback.onFileFound(path);
        }
    }

    private void processDirectory(File directory) {
        String[] files = directory.list();

        if (files == null) {
            callback.onError(directory);
            return;
        }

        for (String file: files) {
            scanRecursively(new File(directory, file));
        }
    }

    public interface FileScannerCallback {
        void onFileFound(File file);
        void onError(File file);
    }
}
