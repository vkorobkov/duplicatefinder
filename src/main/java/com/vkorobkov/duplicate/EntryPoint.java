package com.vkorobkov.duplicate;

import com.vkorobkov.duplicate.util.DirectoryScanner;
import com.vkorobkov.duplicate.util.FastQueue;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.procedure.TLongObjectProcedure;

import java.io.IOException;

public class EntryPoint {

    public static void main(String[] args) {
        long startDate = System.currentTimeMillis();

        final String DIR = "/home/agile/";
        final boolean SEND_ERRORS_TO_OUTPUT = false;

        // Storage for the files which have the same size
        final GroupedBySizeFilesStorage sameSizeFiles = new GroupedBySizeFilesStorage();

        // Scanning directory for the files.
        System.out.println("Looking up for the files");
        new DirectoryScanner(DIR, new FileScannerCallbackImpl(SEND_ERRORS_TO_OUTPUT, sameSizeFiles)).scan();

        // Getting files with the same CRC
        System.out.println("Counting CRC32 of the files we've found");

        SimilarFilesProcessor filesProcessor = new SimilarFilesProcessor();
        Statistics statistics = new Statistics();
        StatisticsPrintingAndCountingProcedure resultsPrinter = new StatisticsPrintingAndCountingProcedure(statistics);
        for (Iterable<String> files: sameSizeFiles) {
            // Getting similar files
            TLongObjectMap<FastQueue<String>> sameFiles = filesProcessor.extractSimilarFiles(files, new SimilarFilesProcessor.ErrorCallback() {
                @Override
                public void onError(String file, IOException exception) {
                    if (SEND_ERRORS_TO_OUTPUT) {
                        System.err.println("Error processing file: " + file + ". Exception: " + exception);
                    }
                }
            });

            // Printing them out and counting the statistics
            sameFiles.forEachEntry(resultsPrinter);
        }

        // Printing files with the same CRC out
        System.out.println("Total number of the same files: " + statistics.totalSameFiles);
        System.out.println("Total number of the scanned files: " + statistics.totalScannedFiles);

        System.out.println("The process took(milliseconds): " + (System.currentTimeMillis() - startDate));
    }

    private static class StatisticsPrintingAndCountingProcedure implements TLongObjectProcedure<FastQueue<String>> {

        private final Statistics statistics;

        private StatisticsPrintingAndCountingProcedure(Statistics statistics) {
            this.statistics = statistics;
        }

        @Override
        public boolean execute(long crc32, FastQueue<String> files) {
            int size = files.size();
            if (size > 1) {
                statistics.totalSameFiles += size;
                for (String file : files) {
                    System.out.println(file);
                }
                System.out.println("=================================================");
            }
            statistics.totalScannedFiles += size;
            return true;
        }
    }

    private static class Statistics {
        public long totalSameFiles;
        public long totalScannedFiles;
    }
}
